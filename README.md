

# Kompilering och körning av kameraservern
Kompilatorn finns i E-husets datorer, så kompilering måste göras från en sådan dator.
Gå till edaf55/c-workspace och kör **make**. Då ska programmet **main** crosskompileras för kameran. Lägg in **main** i en kamera med hjälp av scp. SSH:a sedan in i kameran och kör **main**.
Exempel:
```
#!bash
$ cd git/edaf55/c-workspace
$ make
$ scp main rt@argus-2.student.lth.se:~/
$ ssh rt@argus-2
$ ./main
```
*Tänk på att detta programmet inte går att köras från en vanlig dator, utan det måste läggas in i en kamera och köras där.

*Detta går att göras hemifrån genom att SSH:a in i skolans datorer (e.g. "ssh dat15xyz@login.student.lth.se)")och köra ovanstående kommandon. För att ansluta till en kamera hemifrån måste dock rätt portar vara öppna i din dator och även router om den har brandvägg (6000 TCP, och slumpmässig UDP). Just nu tilldelas klienten en slumpmässig port vilket gör att man inte vet vilken port som ska öppnas förrän man startar programmet. Man skulle kunna öppna en range av portar t.ex. 10000-65000, men detta är inte att rekommendera av säkerhetsskäl.* 

## Motion detection och HTTP server ##
För att använda motion detection och http server behöver ytterligare två program läggas till servern. Gå till /edaf55/c-workspace/examples/ och kör make. Då fås filerna http_server och motion_server. Använd scp för att lägga in dem i kameran. Kör sedan programmen från varsin terminal. 
Exempel:
```
#!bash
$ cd git/edaf55/c-workspace/examples
$ make
$ scp http_server motion_server rt@argus-2.student.lth.se:~/
```


# Kompilering och körning av fake-kameraservern
Gå till mappen edaf55/c-workspace och kör kommandot make -f Makefile.fake. Då ska två program (main1 och main2) kompileras om inget går fel. Main1 använder port 6000 och Main2 använder port 6001. Exempel:
```
#!bash
$ cd git/edaf55/c-workspace
$ make -f Makefile.fake
$ ./main1
```
Sedan är det bara att starta Java-programmet och välj servarna "Local (Port 6000)" och "Local (Port 6001).
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <pthread.h>

enum Mode {MOVIE, IDLE};


struct SharedData{
        int isRunning;
        enum Mode mode;
        struct sockaddr_in clientAddr;
};

void monitor_init();
enum Mode monitor_getMode();
void monitor_setMode(enum Mode m);
struct sockaddr_in monitor_getClientAddr();
void monitor_setClientAddr(struct sockaddr_in addr);
int monitor_isRunning();
void monitor_setRunning(int i);
void monitor_wait();
void monitor_timedwait(long ms);
void monitor_notify();

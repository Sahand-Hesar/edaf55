#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "monitor.h"

#ifndef PORT
#define PORT 6000
#endif
#define IDLE_MSG 0x01
#define MOVIE_MSG 0x02

int waitForConnection(unsigned short port);
void listenForCommands(int clientfd);
void closeConnection(int clientfd);

/*
Waits for the client to request a connection. Once a connection is established, it notifies the Sender thread which will start sending images. It then listens for commands from the client.
 */
void receiver_start(void *vargp){
        while(1){
                int clientfd = waitForConnection(PORT);
                listenForCommands(clientfd);
        }
}

void listenForCommands(int clientfd){
        char buf[1];
        while((read(clientfd, buf, sizeof(buf))) > 0){
                switch(buf[0]){
                case IDLE_MSG: printf("Received Idle-command\n"); monitor_setMode(IDLE); break;
                case MOVIE_MSG: printf("Received Movie-command\n"); monitor_setMode(MOVIE); break;
                default: printf("Received invalid command\n"); break;
                }
                monitor_notify();
        }
        closeConnection(clientfd);
}

void closeConnection(int clientfd){
        monitor_setRunning(0);
        monitor_setMode(IDLE);
        close(clientfd);
        printf("Connection closed.\n");
}

int waitForConnection(unsigned short port)
{
        printf("Waiting for connection at port %u...\n", port);

        struct sockaddr_in servaddr;
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr.sin_port = htons(port);

        int listenfd = socket(AF_INET, SOCK_STREAM, 0);

        int optval = 1;
        if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval))){
                printf("ERROR: setsockopt()");
                return -1;
        }

        if(bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
                printf("ERROR: bind()");
                return -1;
        }

        if(listen(listenfd, 1024) < 0){
                printf("ERROR: listen()");
                return -1;
        }

        int clientfd;
        struct sockaddr_in clientaddr;
        int clientlen = sizeof(clientaddr);
        if((clientfd = accept(listenfd, (struct sockaddr *) &clientaddr, &clientlen)) < 0){
                printf("ERROR: failed to accept client\n" );
                return -1;
        }
        printf("Connection established with client %s:%d!\n", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));

        monitor_setClientAddr(clientaddr);
        monitor_setRunning(1);
        monitor_notify();

        close(listenfd);
        return clientfd;
}

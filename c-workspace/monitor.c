#include <stdio.h>
#include <errno.h>
#include <sys/time.h>

#include "monitor.h"


static pthread_mutex_t mutex;
static pthread_cond_t cond;
static struct SharedData sharedData;

void monitor_init(){
        sharedData.isRunning = 0;
        sharedData.mode = IDLE;
        pthread_mutex_init(&mutex, NULL);
        pthread_cond_init(&cond, NULL);
}
enum Mode monitor_getMode(){
        enum Mode mode;
        pthread_mutex_lock(&mutex);
        mode = sharedData.mode;
        pthread_mutex_unlock(&mutex);
        return mode;
}

void monitor_setMode(enum Mode m){
        pthread_mutex_lock(&mutex);
        sharedData.mode = m;
        pthread_mutex_unlock(&mutex);
}

struct sockaddr_in monitor_getClientAddr(){
        struct sockaddr_in res;
        pthread_mutex_lock(&mutex);
        res = sharedData.clientAddr;
        pthread_mutex_unlock(&mutex);
        return res;
}

void monitor_setClientAddr(struct sockaddr_in addr){
        pthread_mutex_lock(&mutex);
        sharedData.clientAddr = addr;
        pthread_mutex_unlock(&mutex);
}

int monitor_isRunning(){
        int res;
        pthread_mutex_lock(&mutex);
        res = sharedData.isRunning;
        pthread_mutex_unlock(&mutex);
        return res;
}

void monitor_setRunning(int i){
        pthread_mutex_lock(&mutex);
        sharedData.isRunning = i;
        pthread_mutex_unlock(&mutex);
}

void monitor_wait(){
        pthread_mutex_lock(&mutex);
        pthread_cond_wait(&cond, &mutex);
        pthread_mutex_unlock(&mutex);
}

void monitor_timedwait(long ms){
        long secondsToWait = ms/1000;
        long msToWait = ms%1000;
        struct timespec waitUntil;
        struct timeval now;
        gettimeofday(&now,NULL);
        waitUntil.tv_sec = now.tv_sec + secondsToWait;
        waitUntil.tv_nsec = now.tv_usec*1000 + msToWait*1000*1000;
        waitUntil.tv_sec += waitUntil.tv_nsec / (1000*1000*1000);
        waitUntil.tv_nsec %= 1000*1000*1000;

        pthread_mutex_lock(&mutex);
        pthread_cond_timedwait(&cond, &mutex, &waitUntil);
        pthread_mutex_unlock(&mutex);
}

void monitor_notify(){
        printf("Monitor: Notify()");
        pthread_mutex_lock(&mutex);
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
}

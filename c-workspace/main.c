#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include "monitor.h"
#include "camera_receiver.h"
#include "camera_sender.h"

int main(){
        monitor_init();
        
        pthread_t senderThread;
        pthread_create(&senderThread, NULL, &sender_start, NULL);

        pthread_t receiverThread;
        pthread_create(&receiverThread, NULL, &receiver_start, NULL);

        pthread_join(receiverThread, NULL);
        exit(0);
}


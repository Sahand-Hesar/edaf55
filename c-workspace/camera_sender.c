#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <limits.h>

#include "examples/camera.h"
#include "monitor.h"

#define MOVIE_MODE_WAIT_MS 33
#define IDLE_MODE_WAIT_MS 3000


int createSocket();
void sendImage();
void senderLoop();

struct sockaddr_in clientAddr;
static camera *cam;

/*
Waits for the receiver thread to open a connection with the client. The receiver will then notify this thread, which in turn will start sending images to the client.
 */
void sender_start(void *vargp){
        while(1){
                monitor_wait();
                senderLoop();
        }
}


void senderLoop(){
        cam = camera_open();
        int timeToWaitMs;
        while(monitor_isRunning()){
                switch(monitor_getMode()){
                case MOVIE: timeToWaitMs = MOVIE_MODE_WAIT_MS; break;
                case IDLE: timeToWaitMs = IDLE_MODE_WAIT_MS; break;
                }
                monitor_timedwait(timeToWaitMs);
                sendImage();
        }
}

void sendImage(){
        int client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        struct sockaddr_in clientAddr = monitor_getClientAddr();
        bind(client, (struct sockaddr *) &clientAddr, sizeof(clientAddr));

        int i;
        frame *f = camera_get_frame(cam);
        size_t size = get_frame_size(f);
        byte *bytes = get_frame_bytes(f);
        unsigned long long timestamp = get_frame_timestamp(f);

        byte sizeBytes[4];
        sizeBytes[0] = (size >> 24) & 0xFF;
        sizeBytes[1] = (size >> 16) & 0xFF;
        sizeBytes[2] = (size >> 8) & 0xFF;
        sizeBytes[3] = size & 0xFF;

        byte timestampBytes[8];
        timestampBytes[0] = (timestamp >> 56) & 0xFF;
        timestampBytes[1] = (timestamp >> 48) & 0xFF;
        timestampBytes[2] = (timestamp >> 40) & 0xFF;
        timestampBytes[3] = (timestamp >> 32) & 0xFF;
        timestampBytes[4] = (timestamp >> 24) & 0xFF;
        timestampBytes[5] = (timestamp >> 16) & 0xFF;
        timestampBytes[6] = (timestamp >> 8) & 0xFF;
        timestampBytes[7] = timestamp & 0xFF;

        byte packet[12 + size];
        memcpy(packet, sizeBytes, sizeof(sizeBytes));
        memcpy(packet+4, timestampBytes, sizeof(timestampBytes));
        memcpy(packet+12, bytes, size);
        int nbytesSent = sendto(client, packet, sizeof(packet), 0, (struct sockaddr *)&clientAddr, sizeof(clientAddr));
        printf("Sender: Sent %u bytes to %s:%u. (Timestamp: %llu)\n", nbytesSent, inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), timestamp);
        frame_free(f);

        close(client);
}


package camera_client;

import java.io.ByteArrayInputStream;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import javafx.scene.image.Image;

/**
This class recieves images from the buffer, determines if they
are to be shown to the left or the right depending on from which
camera the image was recieved, and passes them on to the GUI.
**/
public class ImageThread extends Thread {

	private Monitor monitor;

	public ImageThread(Monitor m) {
		monitor = m;
	}

	public void run() {
		while (!Thread.interrupted()) {
			monitor.frameWait();
			ImageData currentImageData = monitor.pollBuffer();
			viewImage(currentImageData);
		}
	}

	public void viewImage(final ImageData imageData) {
		if (imageData == null) {
			return;
		}

		SwingWorker<ImageData, String> sw = new SwingWorker<ImageData, String>() {
			@Override
			protected ImageData doInBackground() throws Exception {
				Image image = new Image(new ByteArrayInputStream(imageData.getData()));
				return imageData.addImage(image);
			}

			@Override
			protected void done() {
				try {
					if (get() == null)
						return;
					int camNum = get().cameraNumber();
					Image img = get().getImage();
					
					if(monitor.isConnected(camNum)){
						monitor.getController().showImage(camNum, img);
					}else{
						monitor.getController().showImage(camNum, null);
					}
					
					long delay = System.currentTimeMillis() - get().getTimestamp(); //Difference between the time the image is displayed and the time it was taken.
					monitor.setDelay(camNum, delay);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		};

		sw.execute();
	}
}

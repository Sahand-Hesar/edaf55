package camera_client;

import java.util.HashMap;

public class Constants {

	public final static int SERVER_PORT = 6000;
	public final static int MOTION_PORT = 9091;
	
	public final static char MODE_OFF = 0x00;
	public final static char MODE_IDLE = 0x01;
	public final static char MODE_MOVIE = 0x02;
	

	public final static HashMap<String, String> MAP_SERVERNAME_ADDRESS = new HashMap<>();
	static{
		MAP_SERVERNAME_ADDRESS.put("Local (Port: " + SERVER_PORT + ")", "127.0.0.1" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Local (Port: " + (SERVER_PORT+1) + ")", "127.0.0.1" + ":" + (SERVER_PORT+1));
		MAP_SERVERNAME_ADDRESS.put("Argus-1", "130.235.34.185" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-2", "130.235.34.186" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-3", "130.235.34.187" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-4", "130.235.34.188" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-5", "130.235.34.189" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-6", "130.235.34.190" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-7", "130.235.34.191" + ":" + SERVER_PORT);
		MAP_SERVERNAME_ADDRESS.put("Argus-8", "130.235.34.192" + ":" + SERVER_PORT);
	}
}

package camera_client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;

import gui.Controller;

public class Monitor {

	private PriorityQueue<ImageData> buff;
	private Mode mode;
	private CameraConnection camConnections[];
	private Controller controller;
	private int synchronTime;
	private boolean synchTimeAuto;

	private long[] deltaTimes;
	private long[] delays;
	private long[] secondsSinceLastMotion;
	private HttpURLConnection[] httpMotionConnections;

	private FrameReceiverThread[] receiverThreads;
	/**
	 * the montior keeps track of the buffer, recives images from FrameReceiverThread
	 * and forward the right images to the ImageThread
	 */
	public Monitor(Controller controller) throws UnknownHostException, IOException {
		buff = new PriorityQueue<>();
		receiverThreads = new FrameReceiverThread[2];
		camConnections = new CameraConnection[2];
		httpMotionConnections = new HttpURLConnection[2];
		secondsSinceLastMotion = new long[]{-1, -1};
		deltaTimes = new long[2];
		delays = new long[2];
		
		mode = Mode.AUTO;
		this.controller = controller;
		synchronTime = 100;
		synchTimeAuto = true;
	}
	/**
	 *  Sets up a connection with a camera
	 */
	synchronized public void connectCam(String serverIP, int serverPort, int camNum) {
		try {
			camConnections[camNum] = new CameraConnection(camNum, serverIP, serverPort);
			CameraConnection connection = camConnections[camNum];

			receiverThreads[camNum] = new FrameReceiverThread(this, connection, camNum);
			receiverThreads[camNum].start();

			setMode(getMode());
			controller.updateViewSuccessfullyConnected(camNum);
		} catch (IOException e) {
			System.err.println("Failed to connect to " + serverIP + ":" + serverPort);
			camConnections[camNum] = null;
			controller.updateViewFailedToConnect(camNum);
		}
	}

	/**
	 * Disconects camera and emptys the buffer of that camera
	 */
	synchronized public void closeCam(int camNum){
		System.out.println("Closing camera " + camNum);
		clearBuffer(camNum);
		if(receiverThreads[camNum] != null){
			receiverThreads[camNum].interrupt();
			receiverThreads[camNum] = null;
		}
		camConnections[camNum] = null;
		controller.updateViewNotConnected(camNum);
	}

	/**
	 *  Adds a new image to the buffer
	 */
	synchronized public void add(byte[] imageBytes, long timeStamp, int cam) {
		//Convert camera time to computer time.
		timeStamp = timeStamp + deltaTimes[cam]; 

		buff.add(new ImageData(imageBytes, timeStamp, cam));
		checkBuffSize();
		notifyAll();
	}


	/**
	 *  Returns the controller
	 */
	synchronized public Controller getController() {
		return controller;
	}
	/**
	 *  returns the next image in line in the buffer
	 */
	synchronized public ImageData pollBuffer() {
		return buff.poll();
	}
	/**
	 *  Waits the ImageThread unit an image should be shown in the GUI
	 */
	synchronized public void frameWait() {
		long dt = 0;
		while ((dt = getTime()) > 0) {
			try {
				wait(dt);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 *  returns the time until an image should be shown in GUI
	 */
	synchronized private long getTime() {
		ImageData m = buff.peek();
		if (m == null)
			return Long.MAX_VALUE; // return max value to wait for images.
		return (m.getTimestamp() + this.synchronTime) - System.currentTimeMillis() ;
	}
	
	synchronized public void setDeltaTime(int camNum, long timeMs){
		deltaTimes[camNum] = timeMs;
		
		//If both deltaTimes are initialized (i.e. both cameras are connected), display their clock difference if they are not synchronized.
		if(deltaTimes[0] > 0 && deltaTimes[1] > 0){
			long timeDiff = deltaTimes[0] - deltaTimes[1];
			if(timeDiff > 500)
				System.out.println("Notification: Cameras are out of sync by: " + timeDiff + " ms.  (Meaning that the left camera's clock is " + timeDiff + " ms behind the right camera)");
		}
	}
	

	/**
	 * Checks the size of the buffer to see if the images should be shown in
	 *  synchronous or asynchronous mode
	 */
	private synchronized void checkBuffSize() {
		if (synchTimeAuto) {
			if (buff.size() > 2) {
				setSynchronTime(500);
			} else {
				setSynchronTime(100);
			}
		}
	}

	/**
	 * Set the time an image should be delayed before shown in GUI
	 */
	public void setSynchronTime(int t) {
		this.synchronTime = t;
	}
	/**
	 * Set the synchTimeAuto to true if synchronous/asynchronous is set
	 * to auto, false if manual
	 */
	public synchronized void setSynchBool(boolean b) {
		synchTimeAuto = b;
	}
	
	
	/**
	 * Emptys the buffer from images
	 */
	synchronized private void clearBuffer(int nr) {
		List<ImageData> imagesToRemove = new ArrayList<>();
		Iterator<ImageData> itr = buff.iterator();
		while(itr.hasNext()){
			ImageData img = itr.next();
			if(img.cameraNumber() == nr){
				imagesToRemove.add(img);
			}
		}
		buff.removeAll(imagesToRemove);
	}

	/**
	 * Returns the current mode.
	 */
	synchronized public Mode getMode() {
		return mode;
	}
	/**
	 * Takes the current mode of Movie/idel/auto
	 * and sends it over the socket to CameraServer.
	 */
	synchronized public void setMode(Mode mode) {
		this.mode = mode;
		for (CameraConnection connection : camConnections) {
			if(connection == null || connection.getTCP() == null){
				continue;
			}
			Socket socket = connection.getTCP();
			try {
				switch(mode){
				case IDLE:
					socket.getOutputStream().write(Constants.MODE_IDLE);
					controller.radioBtnIdle.setSelected(true);
					break;
				case MOVIE:
					socket.getOutputStream().write(Constants.MODE_MOVIE);
					controller.radioBtnMovie.setSelected(true);
					break;
				case AUTO:
					socket.getOutputStream().write(Constants.MODE_IDLE);
					controller.radioBtnAuto.setSelected(true);
					break;
				}
			} catch (IOException e) {
				//e.printStackTrace();
				System.out.println("Error: Failed to send command to server. Is it offline?");
				closeCam(connection.getNum());
			}
		}
	}

	synchronized public Optional<CameraConnection> getCameraConnection(int camNr){
		if(camNr < 0 || camNr >= camConnections.length || camConnections[camNr] == null){
			return Optional.empty();
		}else{
			return Optional.of(camConnections[camNr]);
		}
	}


	/**
	 *Forwards the movie/idel mode to the CameraServer.
	 */
	synchronized public void motionDetected(int camNr){
		setMode(Mode.MOVIE);
	}

	synchronized public boolean isConnected(int camNr){
		if(camConnections[camNr] == null){
			return false;
		}else{
			return camConnections[camNr].getTCP().isConnected();
		}
	}

	synchronized public void setDelay(int camNum, long delay) {
		delays[camNum] = delay;
	}
	
	synchronized public long getDelay(int camNum) {
		return delays[camNum];
	}
	synchronized public long getSecondsSinceLastMotion(int camNum) {
		return secondsSinceLastMotion[camNum];
	}
	
	synchronized public void setSecondsSinceLastMotion(int camNum, long secondsSinceLastMotion) {
		this.secondsSinceLastMotion[camNum] = secondsSinceLastMotion;
	}
	synchronized public HttpURLConnection getHttpMotionConnection(int camNum) {
		return httpMotionConnections[camNum];
	}
	synchronized public void setHttpMotionConnection(int camNum, HttpURLConnection httpMotionConnection) {
		httpMotionConnections[camNum] = httpMotionConnection;
	}
}


package camera_client;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class CameraConnection {

	private final Socket tcpSocket;
	private final DatagramSocket udpSocket;
	private final int camNum;
/*
 * connects the client and a camera through sockets
 */
	public CameraConnection(int camNum, String serverIP, int serverPort) throws UnknownHostException, IOException{
		this.tcpSocket = new Socket(serverIP, serverPort);
		this.udpSocket = new DatagramSocket(tcpSocket.getLocalPort());
		this.camNum = camNum;
	}
/*
 * returns the TCP socket
 */
	public Socket getTCP() {
		return tcpSocket;
	}
/*
 * returns the UDP socket
 */
	public DatagramSocket getUDP() {
		return udpSocket;
	}

/*
 * returns the number of the camera
 */
	public int getNum() {
		return camNum;
	}
/*
 * closes both UDP and TCP socket if connected
 */
	public void close() {
		try {
			getUDP().close();
			getTCP().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String toString(){
		return (tcpSocket.getInetAddress().toString().substring(1)) + ":" + tcpSocket.getPort();
	}
}

package camera_client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class FrameReceiverThread extends Thread {

	private final int BUFFER_SIZE = 50000;
	private Monitor monitor;
	private int camNum;
	private CameraConnection connection;

	public FrameReceiverThread(Monitor monitor, CameraConnection connection, int camNum) {
		this.monitor = monitor;
		this.camNum = camNum;
		this.connection = connection;
	}

	public void run() {
		byte[] buffer = new byte[BUFFER_SIZE];
		boolean isFirstIteration = true;
		while (true && !isInterrupted()) {
			try {
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				connection.getUDP().receive(packet);
				if (isInterrupted()) {
					break;
				}

				byte[] payloadSizeBytes = Arrays.copyOf(buffer, 4);
				ByteBuffer bb = ByteBuffer.wrap(payloadSizeBytes);
				bb.order(ByteOrder.BIG_ENDIAN);
				int payloadSize = bb.getInt();

				byte[] timestampBytes = Arrays.copyOfRange(buffer, 4, 12);
				bb = ByteBuffer.wrap(timestampBytes);
				bb.order(ByteOrder.BIG_ENDIAN);
				long timestamp = (long) (bb.getLong() / (1e+6)); // Convert nanoseconds to milliseconds.
				byte[] imageBytes = Arrays.copyOfRange(buffer, 12, payloadSize + 12);

				if(isFirstIteration){
					long deltaTime = System.currentTimeMillis() - timestamp;
					monitor.setDeltaTime(camNum, deltaTime);
					isFirstIteration = false;
				}
				monitor.add(imageBytes, timestamp, camNum);
			} catch (SocketException e) {
				//Exception is thrown when thread is interrupted while waiting on receive() method of the socket.
			} catch (IOException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void interrupt(){
		super.interrupt();  
		connection.close();
		monitor.setDeltaTime(camNum, -1);
		System.out.println("Receiver thread " + connection.getNum() + " closed");
	}
}

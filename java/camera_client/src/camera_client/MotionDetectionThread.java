package camera_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 
 * A thread that is polling the camera's motion detector.
 *
 */
public class MotionDetectionThread extends Thread {

	private Monitor monitor;
	private final long POLLING_RATE_MS = 1000;

	public MotionDetectionThread(Monitor monitor) {
		this.monitor = monitor;
	}

	public void run() {
		while (true) {
			long t0 = System.currentTimeMillis();
			for(int camNum = 0; camNum <= 1; camNum++){
				CameraConnection cc = monitor.getCameraConnection(camNum).orElse(null);
				if(cc == null){
					monitor.setHttpMotionConnection(camNum, null);
				}else{
					String ip = cc.getTCP().getInetAddress().getHostAddress();
					long secondsSinceLastMotion = pollMotion(camNum, "http://" + ip + ":" + Constants.MOTION_PORT);

					if(monitor.getMode() == Mode.AUTO && secondsSinceLastMotion <= 3){
						monitor.motionDetected(cc.getNum());
					}
				}

			}
			
			long t1 = System.currentTimeMillis();
			long timeTaken = t1 - t0;
			try {
				if(timeTaken < POLLING_RATE_MS){
					Thread.sleep(POLLING_RATE_MS - timeTaken);
				}
			} catch (InterruptedException e) {
			}
		}
	}

	private long pollMotion(int camNum, String urlString) {
		long secondsSinceLastMotion;
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String data;
			data = reader.readLine();
			secondsSinceLastMotion = Integer.parseInt(data.split(":")[0]);

			monitor.setHttpMotionConnection(camNum, connection);
			monitor.setSecondsSinceLastMotion(camNum, secondsSinceLastMotion);
		} catch (IOException e) {
			secondsSinceLastMotion = Long.MAX_VALUE;
			monitor.setHttpMotionConnection(camNum, null);
			monitor.setSecondsSinceLastMotion(camNum, secondsSinceLastMotion);
		}

		return secondsSinceLastMotion;
	}


}

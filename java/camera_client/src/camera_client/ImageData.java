package camera_client;

import javafx.scene.image.Image;

public class ImageData implements Comparable<ImageData> {

	private byte[] img;
	private long timeStampMs;
	private Image image;
	private int cameraNum;
	/**
	 * An ImageData object keeps track of all data an image contains 
	 */
	public ImageData(byte[] imgData, long timestampMs, int cam) {
		img = imgData;
		timeStampMs = timestampMs;
		cameraNum = cam;
	}
	/**
	 * compares two different images depending on their timestamp so the
	 * priorityQueue will be correctly sorted
	 **/
	@Override
	public int compareTo(ImageData o) {
		if (o == null) {
			return 0;
		}
		if (timeStampMs == o.timeStampMs) {
			return 0;
		} else if (timeStampMs > o.timeStampMs) {
			return 1;
		} else {
			return -1;
		}
	}
	/**
	 * returns an array containing all the image data
	 */
	public byte[] getData() {
		return img;
	}

	/**
	 * @return The frame's timestamp in milliseconds.
	 */
	public long getTimestamp() {
		return timeStampMs;
	}
 /**
  * sets the image to the prameter and return this ImageData object
	*/
	public ImageData addImage(Image img) {
		image = img;
		return this;
	}
 /**
  * returns the image
	*/
	public Image getImage() {
		return image;
	}
 /**
  * returns the number of the camera
	*/
	public int cameraNumber() {
		return cameraNum;
	}
	/**
	 * returns the timestamp as a string instead of a long
	 */
	public String toString(){
		return "{" + cameraNum + "; " + getTimestamp() + "}";
	}
}

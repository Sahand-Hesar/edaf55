package gui;

import java.net.HttpURLConnection;

import camera_client.Monitor;

/**
 * 
 * A thread that periodically updates the info Texts of the GUI.
 *
 */
public class GUIInfoThread extends Thread{

	private final long REFRESH_RATE_MS = 1000;
	private Monitor monitor;
	private Controller controller;

	public GUIInfoThread(Monitor monitor, Controller controller){
		this.monitor = monitor;
		this.controller = controller;
	}

	public void run(){
		while(true){
			long t0 = System.currentTimeMillis();
			for(int camNum = 0; camNum <= 1; camNum++){
				if(monitor.isConnected(camNum)){
					controller.textsImageServer[camNum].setText(monitor.getCameraConnection(camNum).map(c -> c.toString()).orElse("Not connected"));			
					controller.textsImageDelay[camNum].setText(monitor.getDelay(camNum) + " ms");
					long secondsSinceLastMotion = monitor.getSecondsSinceLastMotion(camNum);


					HttpURLConnection connection = monitor.getHttpMotionConnection(camNum);
					if(connection != null){
						controller.textsMotionServer[camNum].setText(connection.getURL().getHost() + ":" + connection.getURL().getPort());
						String motionString = (secondsSinceLastMotion < 0 || secondsSinceLastMotion > 5000) ? "No" : secondsSinceLastMotion + " seconds ago";
						controller.textsMotionDetected[camNum].setText(motionString);
					}else{
						controller.textsMotionServer[camNum].setText("Offline");
					}
				}else{
					controller.textsImageServer[camNum].setText("Not connected");
					controller.textsMotionServer[camNum].setText("Not connected");
					controller.textsImageDelay[camNum].setText("N/A");
					controller.textsMotionDetected[camNum].setText("N/A");
				}
			}
			try {
				long t1 = System.currentTimeMillis();
				long timeTaken = t1 - t0;
				if(timeTaken < REFRESH_RATE_MS){
					Thread.sleep(REFRESH_RATE_MS - timeTaken);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

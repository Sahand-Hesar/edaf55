package gui;

import static camera_client.Constants.*;

import java.util.Collections;

import camera_client.Constants;
import camera_client.Mode;
import camera_client.Monitor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

/*
This class contains all the functionality of the GUI, and connects it
to the rest of the program.
*/
public class Controller {
	
	public Text textMessageLeft;
	public Text textMessageRight;
	public Text[] textsMessage;
	
	public Text textImageServerLeft;
	public Text textImageServerRight;
	public Text[] textsImageServer;
	
	public Text textMotionServerLeft;
	public Text textMotionServerRight;
	public Text[] textsMotionServer;
	
	public Text textImageDelayLeft;
	public Text textImageDelayRight;
	public Text[] textsImageDelay;
	
	public Text textMotionDetectedLeft;
	public Text textMotionDetectedRight;
	public Text[] textsMotionDetected;
	
	
	public ImageView imageViewLeft;
	public ImageView imageViewRight;
	public ImageView[] imageViews;

	public ComboBox<String> comboBoxServerLeft;
	public ComboBox<String> comboBoxServerRight;
	public ComboBox<String>[] comboBoxServers;

	@FXML
	public ToggleGroup modeGroup;
	@FXML
	public RadioButton radioBtnMovie;
	@FXML
	public RadioButton radioBtnIdle;
	@FXML
	public RadioButton radioBtnAuto;

	@FXML
	public ToggleGroup syncGroup;
	@FXML
	public RadioButton radioBtnSynchronous;
	@FXML
	public RadioButton radioBtnAsynchronous;
	@FXML
	public RadioButton radioBtnAutoSync;

	public Button btnConnectLeft;
	public Button btnConnectRight;
	public Button[] buttonsConnect;
	
	public Button btnDisconnectRight;
	public Button btnDisconnectLeft;
	public Button[] buttonsDisconnect;

	private Monitor monitor;

	/*
	 * Initializes the controls and fields of the GUI
	 */
	public void initialize() {
		textsMessage = new Text[]{textMessageLeft, textMessageRight};
		textsImageServer = new Text[]{textImageServerLeft, textImageServerRight};
		textsMotionServer = new Text[]{textMotionServerLeft, textMotionServerRight};
		textsImageDelay = new Text[]{textImageDelayLeft, textImageDelayRight};
		textsMotionDetected = new Text[]{textMotionDetectedLeft, textMotionDetectedRight};
		imageViews = new ImageView[]{imageViewLeft, imageViewRight};
		comboBoxServers = new ComboBox[]{comboBoxServerLeft, comboBoxServerRight};
		buttonsConnect = new Button[]{btnConnectLeft, btnConnectRight};
		buttonsDisconnect = new Button[]{btnDisconnectLeft, btnDisconnectRight};
		
		btnDisconnectLeft.setDisable(true);
		btnDisconnectRight.setDisable(true);
		modeGroup = new ToggleGroup();
		radioBtnMovie.setToggleGroup(modeGroup);
		radioBtnIdle.setToggleGroup(modeGroup);
		radioBtnAuto.setToggleGroup(modeGroup);

		syncGroup = new ToggleGroup();
		radioBtnSynchronous.setToggleGroup(syncGroup);
		radioBtnAsynchronous.setToggleGroup(syncGroup);
		radioBtnAutoSync.setToggleGroup(syncGroup);

		radioBtnIdle.setSelected(true);

		imageViewLeft.setPreserveRatio(false);
		imageViewRight.setPreserveRatio(false);
		textMessageLeft.setText("");
		textMessageRight.setText("");
		radioBtnAuto.setSelected(true);
		radioBtnAutoSync.setSelected(true);

		ObservableList<String> options = FXCollections.observableArrayList();
		for (String s : MAP_SERVERNAME_ADDRESS.keySet()) {
			options.add(s);
		}
		Collections.sort(options);
		comboBoxServerLeft.setItems(options);
		comboBoxServerRight.setItems(options);
		comboBoxServerLeft.getSelectionModel().select(0);
		comboBoxServerRight.getSelectionModel().select(1);
	}

	/*
	 * Connects the left image field to a camera
	 */
	public void connectLeft() {
		connect(0);
	}

	/*
	 * Connects the right image field to a camera
	 */
	public void connectRight() {
		connect(1);
	}
	
	private void connect(int camNum){
		String server = comboBoxServers[camNum].getSelectionModel().getSelectedItem();
		String address = MAP_SERVERNAME_ADDRESS.get(server);
		System.out.println("Connecting to " + server + " (" + address + ")");
		String ip = address.split(":")[0];
		int port = Integer.parseInt(address.split(":")[1]);
		monitor.connectCam(ip, port, camNum);
	}
	
	/*
	 * Disconnects the left image field from a camera
	 */
	public void disconnectLeft() {
		monitor.closeCam(0);
	}


	/*
	 * Disconnects the right image field from a camera
	 */
	public void disconnectRight() {
		monitor.closeCam(1);
	}

	
	public void showImage(int camNr, Image image){
		if(camNr < 0 || camNr >= imageViews.length){
			return;
		}
		imageViews[camNr].setImage(image);
	}

	public void setMonitor(Monitor monitor) {
		this.monitor = monitor;
	}

	/*
	 * Sets the mode to "Idle"
	 */
	public void idle() {
		monitor.setMode(Mode.IDLE);
	}

	/*
	 * Sets the mode to "Movie"
	 */
	public void movie() {
		monitor.setMode(Mode.MOVIE);
	}

	/*
	 * Sets the mode to "Auto"
	 */
	public void auto() {
		monitor.setMode(Mode.AUTO);
	}

	/*
	 * Sets the sync mode to "Synchronus"
	 */
	public void synchronousMode() {
		monitor.setSynchBool(false);
		monitor.setSynchronTime(500);
	}

	/*
	 * Sets the sync mode to "Asynchronus"
	 */
	public void asynchronousMode() {
		monitor.setSynchBool(false);
		monitor.setSynchronTime(100);

	}

	/*
	 * Sets the sync mode to "Auto"
	 */
	public void autoSynchronous() {
		monitor.setSynchBool(true);
	}

	/*
		Updates buttons to make sure the user can't click the wrong button when connected / disconnected.
	*/
	public void updateViewSuccessfullyConnected(int camNum) {
		textsMessage[camNum].setText("");
		buttonsConnect[camNum].setDisable(true);
		comboBoxServers[camNum].setDisable(true);
		buttonsDisconnect[camNum].setDisable(false);
	}

	/*
	 * Updates a text pannel to show that the connecion have failed to the user.
	 */
	public void updateViewFailedToConnect(int camNum) {
		showMessage(textsMessage[camNum], "Failed to connect", Paint.valueOf("#ff0000"));
		updateViewNotConnected(camNum);
	}	
	
	public void updateViewNotConnected(int camNum) {
		buttonsConnect[camNum].setDisable(false);
		comboBoxServers[camNum].setDisable(false);
		buttonsDisconnect[camNum].setDisable(true);
		showImage(camNum, null);
	}	

	/*
	 * Text that will be displayed with given color to given text pannel.
	 */
	public void showMessage(Text text, String msg, Paint color) {
		text.setFill(color);
		text.setText(msg);
	}




}

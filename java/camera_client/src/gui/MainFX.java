package gui;

import camera_client.ImageThread;
import camera_client.Monitor;
import camera_client.MotionDetectionThread;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainFX extends Application{

	public static void main(String[] args){
		launch(args);
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
		//scene.getStylesheets().add("style.css");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		Controller controller = loader.getController();
		Monitor monitor = new Monitor(controller);
		controller.setMonitor(monitor);
		controller.initialize();
		
		GUIInfoThread guiInfoThread = new GUIInfoThread(monitor, controller);
		MotionDetectionThread commandSender = new MotionDetectionThread(monitor);
		ImageThread imgThread = new ImageThread(monitor);

		commandSender.start();
		imgThread.start();
		guiInfoThread.start();
	}

}
